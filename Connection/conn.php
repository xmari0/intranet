<?php
/**
 * Created by PhpStorm.
 * User: Mario Zimmermann
 * Date: 10.11.2016
 * Time: 22:31
 */
// connection
$db_host = 'localhost';
$db_name = 'test';
$db_user = 'root';
$db_pass = '';

// mysqli object
$connect = new mysqli ($db_host, $db_user, $db_pass, $db_name);

// error
if ($connect->connect_error) {
    printf ("Connection failed: %s\n", $connect->connect_error);
    exit();
}
?>
<?php
// check if submitted
if (isset ($_POST['submit'])) {
    $title = mysqli_real_escape_string($connect, $_POST['title']);
    $link = mysqli_real_escape_string($connect, $_POST['link']);
    $description = mysqli_real_escape_string($connect, $_POST['description']);
}
//query
$query = "INSERT INTO employees (title, link, description) VALUES('$_POST[title]','$_POST[link]','$_POST[description]')";
$sql = mysqli_query($connect, $query);

mysqli_close($connect);
?>