<?php
require ('Connection/conn.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form test</title>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap-fileinput/js/fileinput.js"></script>
    <script src="bower_components/bootstrap-fileinput/js/locales/de.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/bootstrap-validator/dist/validator.min.js"></script>

    <link href="bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet" type="text/css">



</head>
<body>
<form data-toggle="validator" method="post" >
    <div class="form-group">
        <label for="inputName" class="control-label">Titel</label>
        <input type="text" name="title" class="form-control" rows="1" id="title" placeholder="Titel" required>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label for="inputName" class="control-label">Link</label>
        <input type="url" name="link" class="form-control" rows="1" id="link" placeholder="Link" data-error="Bitte eine gültige URL angeben. (http://)" required>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label for="inputName" class="control-label">Beschreibung</label>
        <textarea class="form-control" name="description" data-minlength="10" rows="2" id="disc" placeholder="Beschreibung" data-error="Mindestens 10 Zeichen." required></textarea>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="control-label">Thumbnail</label>
        <input id="input-de" name="inputde[]" type="file" class="file" data-show-preview="false">
    </div>
</form>

<script>
    $("#input-de").fileinput({
        language: "de"
    })
</script>
</body>

</html>